FROM alpine:3.8


COPY xtremecloud-sso /xtremecloud-sso
COPY xtremecloud-sso-gcp /xtremecloud-sso-gcp
COPY xtremecloud-sso-azure /xtremecloud-sso-azure
COPY xtremecloud-sso-ibmcloud /xtremcloud-sso-ibmcloud
COPY xtremecloud-sso-oraclecloud /xtremecloud-sso-oraclecloud
CMD ["sh"]
